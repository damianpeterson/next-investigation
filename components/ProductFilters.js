import React from 'react';
import PropTypes from 'prop-types';

export default class ProductFilters extends React.PureComponent {
  render() {
    const { onFilterSelect, sortOrder } = this.props;
    return (
      <div className="product-filters">
        <div className="container">
          <div className="select">
            <select className="select--text" value={sortOrder} required onChange={e => onFilterSelect(e)}>
              <option value="alphabetical">Alphabetical</option>
              <option value="sale">On sale</option>
              <option value="lowest">Lowest Price</option>
              <option value="highest">Highest Price</option>
            </select>
            <span className="select--highlight" />
            <span className="select--bar" />
            <label className="select--label">Sort By</label>
          </div>
        </div>
      </div>
    );
  }
}

ProductFilters.propTypes = {
  onFilterSelect: PropTypes.func.isRequired,
  sortOrder: PropTypes.string.isRequired
};
