import PropTypes from 'prop-types';
import React from 'react';
import unfetch from 'isomorphic-unfetch';
import ProductFilters from './ProductFilters';
import ProductList from './ProductList';
import ProductPagination from './ProductPagination';
import { endpoints, sortProducts } from '../library/productHelpers';
import { productPropTypes } from './ProductItem';

export default class CategoryPage extends React.Component {
  /**
   * Set up the initial state from the props we are provided
   * @param props
   */
  constructor(props) {
    super(props);
    const sortedProducts = sortProducts('alphabetical', props.products || []);
    this.state = {
      products: sortedProducts || [],
      currentPage: 0,
      sortOrder: 'alphabetical'
    };

    this.handlePager = this.handlePager.bind(this);
    this.handleFilterSelect = this.handleFilterSelect.bind(this);
  }

  /**
   * When the next page is clicked this will go get a different json file
   * @param pageNumber
   * @returns {Promise<void>}
   */
  async handlePager(pageNumber) {
    const res = await unfetch(endpoints[2], { credentials: 'omit' });
    const json = await res.json();
    const { sortOrder } = this.state;
    this.setState({
      products: sortProducts(sortOrder, json),
      currentPage: pageNumber
    });
  }

  /**
   * When the filter is used this sorts the products and sets the current sort order
   * @param e
   */
  handleFilterSelect(e) {
    const { products } = this.state;
    const sortedProducts = sortProducts(e.target.value, products);

    this.setState({
      products: sortedProducts,
      sortOrder: e.target.value
    });
  }

  /**
   * Renders the page. In Next it uses _document.js as a base. Client-side it just provides a div
   * @returns {*}
   */
  render() {
    const { products, currentPage, sortOrder } = this.state;
    return (
      <div>
        <header>
          <h1>Showing {products.length} products</h1>
        </header>
        <ProductFilters onFilterSelect={this.handleFilterSelect} sortOrder={sortOrder} />
        <ProductList products={products} />
        <ProductPagination onPaginationClick={this.handlePager} currentPage={currentPage} />
        <footer />
      </div>
    );
  }
}

CategoryPage.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape(productPropTypes).isRequired).isRequired
};
