import PropTypes from 'prop-types';
import React from 'react';
import ProductItem, { productPropTypes } from './ProductItem';

export default class ProductList extends React.PureComponent {
  render() {
    const { products } = this.props;
    return (
      <div className="container product-list">
        {products.map(product => {
          return <ProductItem product={product} key={product.productId} />;
        })}
      </div>
    );
  }
}

ProductList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape(productPropTypes).isRequired).isRequired
};
