import PropTypes from 'prop-types';
import React from 'react';

export default class ProductPagination extends React.PureComponent {
  render() {
    const { currentPage, onPaginationClick } = this.props;
    return (
      <div className="container product-pagination">
        <div className="pagination">
          <ul>
            <li>
              <button type="button" disabled={currentPage === 0} onClick={() => onPaginationClick(0)}>
                1
              </button>
            </li>
            <li>
              <button type="button" disabled={currentPage === 1} onClick={() => onPaginationClick(1)}>
                2
              </button>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

ProductPagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
  onPaginationClick: PropTypes.func.isRequired
};
