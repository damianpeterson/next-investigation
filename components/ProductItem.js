import PropTypes from 'prop-types';
import React from 'react';

/**
 * This is used EVERYWHERE so export it
 * @type {{productId: shim, price: shim, imageUrl: shim, description: shim}}
 */
export const productPropTypes = {
  productId: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  inSale: PropTypes.bool
};

export default class ProductItem extends React.PureComponent {
  render() {
    const { product } = this.props;
    let className = 'product-list--item';
    if (product.inSale === true) {
      className += ' in-sale';
    }
    return (
      <div className={className}>
        <a href={`/product?id=${product.productId}`}>
          <img src={product.imageUrl} alt={product.description} />
          <h2>{product.description}</h2>
          <h3>
            {'$'}
            {product.price}
          </h3>
        </a>
      </div>
    );
  }
}

ProductItem.propTypes = {
  product: PropTypes.shape(productPropTypes).isRequired
};
