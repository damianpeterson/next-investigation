/**
 * These two endpoints are used in lieu of having an actual API endpoint we can use. Two pages of products.
 * @type {string[]}
 */
export const endpoints = [
  'https://s3.eu-west-2.amazonaws.com/peterson.london/category.json',
  'https://s3.eu-west-2.amazonaws.com/peterson.london/category2.json',
  'http://jdwsearchapi-env-1.mjyhffnp8v.us-east-2.elasticbeanstalk.com/products'
];

/**
 * Sorts products by alphabetical, lowest price and highest price
 * @param sortOrder
 * @param products
 * @returns {this}
 */
export function sortProducts(sortOrder, products) {
  let sortedProducts;

  switch (sortOrder) {
    case 'alphabetical':
      sortedProducts = [...products].sort((a, b) => {
        return a.description > b.description ? 1 : -1;
      });
      break;
    case 'sale':
      sortedProducts = [...products].sort((a, b) => {
        return b.inSale > a.inSale ? 1 : -1;
      });
      break;
    case 'lowest':
      sortedProducts = [...products].sort((a, b) => {
        return parseFloat(a.price) - parseFloat(b.price);
      });
      break;
    case 'highest':
      sortedProducts = [...products].sort((a, b) => {
        return parseFloat(b.price) - parseFloat(a.price);
      });
      break;
    default:
      sortedProducts = products;
  }

  return sortedProducts;
}
