import React from 'react';
import unfetch from 'isomorphic-unfetch';
import { endpoints } from '../library/productHelpers';
import { productPropTypes } from '../components/ProductItem';

export default class ProductPage extends React.Component {
  static async getInitialProps({ query }) {
    const res = await unfetch(`${endpoints[2]}/${query.id}`);
    const json = await res.json();
    return { product: json };
  }

  constructor(props) {
    super(props);
    this.state = props;
  }

  render() {
    const { product } = this.state;
    return (
      <div className="container container-grid product-page">
        <div className="column">
          <img src={product.imageUrl} alt={product.description} />
        </div>
        <div className="column">
          <div className="product-page--content">
            <div className="product-page--details">
              <h1>{product.description}</h1>
              <h2>
                {'$'}
                {product.price}
              </h2>
            </div>
            <div className="product-page--refinements">
              <div className="select">
                <select className="select--text" required>
                  <option value="" disabled />
                  <option value="1">10</option>
                  <option value="2">12</option>
                  <option value="3">14</option>
                </select>
                <span className="select--highlight" />
                <span className="select--bar" />
                <label className="select--label">Select Size</label>
              </div>
              <button>Add To Bag</button>
            </div>
            <div className="product-page--description">
              <p>
                Look great in this crinkle pull-on bardot. The bardot is fashioned from a soft crinkle fabric and
                features a frill detail to the front. Available in a variety of prints and colours. The crinkle bardot
                is perfect to be worn in those warmer summer months, worn with trousers and sandals.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProductPage.propTypes = productPropTypes;
