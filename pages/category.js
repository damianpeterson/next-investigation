import PropTypes from 'prop-types';
import React from 'react';
import Head from 'next/head';
import unfetch from 'isomorphic-unfetch';
import CategoryPage from '../components/CategoryPage';
import { endpoints, sortProducts } from '../library/productHelpers';
import { productPropTypes } from '../components/ProductItem';

/**
 * The component that is used either as a whole page in Next or just a bit of the page client-side
 */
export default class SSRCategoryPage extends React.Component {
  /**
   * This only fires for NextJS when it renders the initial HTML
   * @returns {Promise<{products}>}
   */
  static async getInitialProps() {
    const res = await unfetch(endpoints[2], { credentials: 'omit' });
    const json = await res.json();
    return { products: json };
  }

  /**
   * Set up the initial state from the props we are provided
   * @param props
   */
  constructor(props) {
    super(props);
    const sortedProducts = sortProducts('alphabetical', props.products || []);
    this.state = {
      products: sortedProducts
    };
  }

  /**
   * Renders the page. In Next it uses _document.js as a base. Client-side it just provides a div
   * @returns {*}
   */
  render() {
    const { products } = this.state;
    return (
      <div>
        <Head>
          <title>Category page</title>
        </Head>
        <CategoryPage products={products} />
        <footer />
      </div>
    );
  }
}

SSRCategoryPage.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape(productPropTypes).isRequired).isRequired
};
