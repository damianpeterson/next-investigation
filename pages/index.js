import React from 'react';
import Head from 'next/head';

export default function() {
  return (
    <div>
      <Head>
        <title>My landing page</title>
      </Head>
      <header>Landing page</header>
      <div className="container">
        <div className="column">
          <a href="/category">Go to category page</a>
        </div>
      </div>
      <footer>Footer</footer>
    </div>
  );
}
