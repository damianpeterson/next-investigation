module.exports = {
  entry: {
    category: './legacy/category.js'
  },
  output: {
    filename: '[name].min.js',
    path: `${__dirname}/static/js`
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
};