import React from 'react';
import ReactDOM from 'react-dom';
import CategoryPage from '../components/CategoryPage';
import { endpoints } from '../library/productHelpers';

fetch(endpoints[2], { credentials: 'omit' })
  .then(response => response.json())
  .then(json => {
    ReactDOM.render(<CategoryPage products={json} />, document.getElementById('app'));
  });
